import 'package:dart_snadbox/sandbox/enums/project.dart';

class Permission {
  final String title;
  final String code;
  final ProjectEnum project;

  bool isActive;

  Permission(this.title, this.code, this.project) : this.isActive = true;

  @override
  int get hashCode {
    return this.code.hashCode + this.project.hashCode;
  }

  @override
  bool operator ==(other) {
    return this.hashCode == other.hashCode;
  }
}
