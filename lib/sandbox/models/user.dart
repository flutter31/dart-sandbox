import 'package:dart_snadbox/sandbox/enums/project.dart';
import 'package:dart_snadbox/sandbox/enums/role.dart';
import 'package:dart_snadbox/sandbox/factories/permissions.dart';
import 'package:dart_snadbox/sandbox/models/permission.dart';

class User {
  final String name;
  final String email;
  final RoleEnum role;

  Set<Permission> permissions;

  User(this.name, this.email, {this.role = RoleEnum.user}) {
    this.permissions = PermissionsFactory.defaultPermissions[this.role];
  }

  Set<ProjectEnum> get projects {
    Set<ProjectEnum> projects = Set.from(this
        .permissions
        .where((permission) => permission.isActive)
        .map((permission) => permission.project)
        .toList());
    return projects;
  }

  int get permissionsCount {
    return this.permissions.where((p) => p.isActive).length;
  }

  void addPermission(Permission permission) {
    this.permissions.add(permission);
  }

  void togglePermission(Permission permission) {
    permission.isActive = !permission.isActive;
    this.permissions = this
        .permissions
        .map(
          (p) => p == permission ? permission : p,
        )
        .toSet();
  }
}
