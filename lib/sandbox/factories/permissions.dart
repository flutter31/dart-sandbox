import 'package:dart_snadbox/sandbox/enums/project.dart';
import 'package:dart_snadbox/sandbox/enums/role.dart';
import 'package:dart_snadbox/sandbox/models/permission.dart';

class PermissionsFactory {
  static List<Permission> _userDefaultPermissions = [
    Permission('View profiles', 'view-facebook-profiles', ProjectEnum.facebook),
    Permission('View profiles', 'view-insta-profiles', ProjectEnum.instagram),
    Permission('View profiles', 'view-linkedin-profiles', ProjectEnum.linkedin),
    Permission('View friends', 'view-facebook-friends', ProjectEnum.facebook),
    Permission('View followers', 'view-insta-followers', ProjectEnum.instagram),
    Permission('View friends', 'view-linkedin-friends', ProjectEnum.linkedin),
  ];

  static List<Permission> _adminDefaultPermissions = [
    ..._userDefaultPermissions,
    Permission(
        'Create profile', 'create-facebook-profiles', ProjectEnum.facebook),
    Permission(
        'Create profile', 'create-insta-profiles', ProjectEnum.instagram),
    Permission(
        'Create profile', 'create-linkedin-profiles', ProjectEnum.linkedin),
  ];

  static List<Permission> _rootAdminDefaultPermissions = [
    ..._adminDefaultPermissions,
    Permission(
        'Delete profile', 'delete-facebook-profiles', ProjectEnum.facebook),
    Permission(
        'Delete profile', 'delete-insta-profiles', ProjectEnum.instagram),
    Permission(
        'Delete profile', 'delete-linkedin-profiles', ProjectEnum.linkedin),
  ];

  static Map<RoleEnum, Set<Permission>> defaultPermissions = {
    RoleEnum.user: Set.from(_userDefaultPermissions),
    RoleEnum.admin: Set.from(_adminDefaultPermissions),
    RoleEnum.root: Set.from(_rootAdminDefaultPermissions)
  };
}
