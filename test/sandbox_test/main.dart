import 'package:dart_snadbox/sandbox/enums/project.dart';
import 'package:dart_snadbox/sandbox/enums/role.dart';
import 'package:dart_snadbox/sandbox/models/permission.dart';
import 'package:dart_snadbox/sandbox/models/user.dart';

import 'package:flutter_test/flutter_test.dart';

void main() {
  group('users tests', () {
    var user = User('Test user', 'test@mail.com', role: RoleEnum.user);
    var admin = User('Test user', 'test@mail.com', role: RoleEnum.admin);
    var rootAdmin = User('Test user', 'test@mail.com');

    test('role test', () {
      expect(user.role, RoleEnum.user);
    });

    test('default permissions test', () {
      expect(admin.projects.length, 3);
    });

    test('adding permission test', () {
      admin.addPermission(Permission(
          'Launch search', 'launch-google-seacrh', ProjectEnum.testProject));
      expect(admin.permissionsCount, 10);
    });

    test('adding permission test', () {
      admin.addPermission(Permission(
          'Launch search', 'launch-google-seacrh', ProjectEnum.google));
      expect(admin.permissionsCount, 11);
    });

    test('adding permission test', () {
      admin.addPermission(Permission(
          'Launch search', 'launch-google-seacrh', ProjectEnum.google));
      expect(admin.projects.length, 5);
    });

    test('adding permission test', () {
      expect(admin.permissionsCount, 11);
    });

    test('positive toggle permission test', () {
      admin.togglePermission(rootAdmin.permissions.last);

      expect(admin.permissionsCount, 10);
    });

    test('negative toggle permission test', () {
      admin.togglePermission(
          Permission('permission 1', 'permission', ProjectEnum.google));

      expect(admin.permissionsCount, 10);
    });

    test('positive permission comparison', () {
      expect(
          Permission('permission 1', 'permission', ProjectEnum.google) ==
              Permission('permission 2', 'permission', ProjectEnum.google),
          true);
    });

    test('negative permission comparison', () {
      expect(
          Permission('permission', 'permission', ProjectEnum.google) ==
              Permission('permission', 'permission-other', ProjectEnum.google),
          false);
    });
  });
}
